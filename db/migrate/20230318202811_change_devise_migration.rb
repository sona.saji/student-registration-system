class ChangeDeviseMigration < ActiveRecord::Migration[7.0]
  def change
    change_column_null :users, :email, true
    change_column_null :users, :phone, true
    remove_index :users, :email
    remove_index :users, :phone
  end
end
