Rails.application.routes.draw do
  devise_for :users,
                 controllers: {
                   sessions: 'users/sessions',
                   registrations: 'users/registrations',
                   passwords: 'users/passwords',
                 }
  devise_scope :user do
    post '/users/register' => 'users/registrations#create'
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  resources :users  

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "home#index"
end
