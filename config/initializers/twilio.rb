require 'twilio-ruby'

# Configure the Twilio client
Twilio.configure do |config|
  config.account_sid = Rails.application.credentials.sms[:account_sid]
  config.auth_token = Rails.application.credentials.sms[:auth_token]
end

# Create a new Twilio client object
$twilio_client = Twilio::REST::Client.new

# Define a method to send a welcome SMS
def send_welcome_sms(user)
  $twilio_client.messages.create(
    from: Rails.application.credentials.sms[:phone_number],
    to: user.phone,
    body: "Hi #{user.first_name.titleize}, Welcome to STUDENT REGISTER!"
  )
end