# frozen_string_literal: true

# Manage athorization for Users
class UserPolicy < ApplicationPolicy

  def admin?
    if (user.id == record.id) && user.has_role?(:admin)
      true
    else
      false
    end
  end

  def admin_or_student?
    if (user.id == record.id) && user.has_any_role?(:admin, :student)
      true
    else
      false
    end
  end

 

  def student?
    if (user.id == record.id) && user.has_role?(:student)
      true
    else
      false
    end
  end


  # Manage athorization scope
  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
