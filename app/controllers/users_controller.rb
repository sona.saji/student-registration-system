class UsersController < ApplicationController
  before_action :set_user, only: %i[ show destroy ]

  def index
    authorize(current_user, :admin?, policy_class: UserPolicy)
    @users = User.all
  end

  def show
    authorize(current_user, :admin_or_student?, policy_class: UserPolicy)
  end

  def destroy
    authorize(current_user, :admin_or_student?, policy_class: UserPolicy)
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url, notice: "User was successfully destroyed." }
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end
end
