module SendWelcomeEmailOrSms
  extend ActiveSupport::Concern

  def send_welcome_sms_and_email
    if params[:contact_method] == "Email"
      UserMailer.welcome_email(@user).deliver_now # Call the send_welcome_mail method
    elsif params[:contact_method] == "Phone"
      send_welcome_sms(@user) # Call the send_welcome_sms method
    else
      UserMailer.welcome_email(@user).deliver_now # Call the send_welcome_mail method
      send_welcome_sms(@user) # Call the send_welcome_sms method
    end
  end
end