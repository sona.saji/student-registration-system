class ApplicationController < ActionController::Base
  include Pundit::Authorization
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!


  private

  def user_not_authorized
    respond_to do |format|
      format.html { redirect_to root_url, notice: "You are not authorized to perform this action." }
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :email, :phone])
    devise_parameter_sanitizer.permit :sign_in, keys: [:login, :password]
  end
end
