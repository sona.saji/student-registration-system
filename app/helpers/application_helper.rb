module ApplicationHelper
  def user_in_and_out
    if user_signed_in?
  
      link_to(' Logout', destroy_user_session_path, class: 'grey-btn glyphicon glyphicon-log-in')
    else
      link_to(' Login', new_user_session_path, method: :delete, class: 'grey-btn glyphicon glyphicon-log-in', title: 'logout')
    end
  end

  # full_name
  def current_user_full_name
    "#{current_user.first_name.titleize} #{current_user.last_name.titleize}"
  end
end
