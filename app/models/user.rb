# frozen_string_literal: true

# # User model for adding users
class User < ApplicationRecord
  rolify
  
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :validatable,
          :authentication_keys => [:login]

  attr_accessor :login

  #validations
  validates :first_name , presence: true
  validates :email, format: Devise.email_regexp, if: -> { email.present? }
  validates :phone, format: { with: /\A\+(?:[0-9]●?){6,14}[0-9]\z/, message: "must be a phone number with country code" }, if: -> { phone.present? }
 
  def login
    @login || self.email || self.phone
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(email) = :value OR phone = :value", { :value => login.downcase }]).first
    else
      where(conditions.to_h).first
    end
  end

  def email_required?
    false
  end
end