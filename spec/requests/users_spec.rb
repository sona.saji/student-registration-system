# frozen_string_literal: true

require 'rails_helper'

RSpec.describe('users', type: :request) do
  let!(:new_user) { create(:user) }

  before do
    new_user.add_role(:student)
    sign_in new_user
  end

  describe '#show' do
    it 'returns user' do
      get user_path(new_user)
      expect(response).to have_http_status(:ok)
    end
  end

  describe '#delete' do
    it 'destroy user' do
      expect do
        delete(user_path(new_user))
      end.to(change(User, :count).by(-1))
    end
  end
end
