# frozen_string_literal: true

require 'faker'
FactoryBot.define do
  factory :user do
    email { Faker::Internet.unique.email }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    phone { Faker::PhoneNumber.cell_phone_in_e164 }
    password { Faker::Alphanumeric.alphanumeric(number: 10) }
    password_confirmation { password }
  end
end
