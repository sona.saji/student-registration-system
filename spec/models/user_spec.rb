# frozen_string_literal: true

require 'rails_helper'

RSpec.describe(User, type: :model) do
  # create user
  let(:new_user) { create :user }

  describe 'create user' do
    it 'new user can be added' do
      expect(new_user).to(be_valid)
      expect(described_class.last.email).to(eq(new_user.email))
    end
  end

  # validates user model
  context 'with users model validation' do
    # validates email/phone
    it 'must have a either email/phone number' do
      new_user = described_class.new(email: '', phone: '').save
      expect(new_user).to(eq(false))
    end

    it 'with email' do
      new_user = described_class.new(email: Faker::Internet.unique.email, phone: '').save
      expect(new_user).to(eq(false))
    end

    it 'with phone number' do
      new_user = described_class.new(email: '', phone: Faker::PhoneNumber.cell_phone_in_e164).save
      expect(new_user).to(eq(false))
    end

    # validates email/phone
    it 'have both email and phone number' do
      new_user = described_class.new(email: Faker::Internet.unique.email , phone: Faker::PhoneNumber.cell_phone_in_e164).save
      expect(new_user).to(eq(false))
    end

    # validates first name
    it 'must have a first name' do
      new_user = described_class.new(first_name: '').save
      expect(new_user).to(eq(false))
    end

    # validates lastname
    it 'have a last name' do
      new_user = described_class.new(last_name: 'student').save
      expect(new_user).to(eq(false))
    end
  end
end
